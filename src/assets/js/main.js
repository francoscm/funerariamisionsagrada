$(document).ready(function(){
	//Example
	loadCSS( "http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,700,300,600,800" );

	new Imager({ availableWidths: [320, 640, 1024] });

	$('.bxslider').bxSlider({
		pause: 4000,
		auto: true
	});
	$('.bxslider_urnas').bxSlider({
		pause: 4000,
		auto: true,
		controls: false
	});	
	$('.bxslider_convenio').bxSlider({		
		minSlides: 3,
		maxSlides: 4,
		slideWidth: 200,
		slideMargin: 10,
		auto: true
	});

	$('.bxslider_carrozas').bxSlider({
		pause: 4000,
		auto: true,
		controls: false
	});	

	$.slidebars();

	$(".fancybox").fancybox({
		openEffect	: 'none',
		closeEffect	: 'none'
	});



	//Validation Contact
	$("#contact").validate({
		  rules: {
		    nombre:{
		    	required: true
		    },
			telefono:{
		    	required: true
		    },		    
		    email: {
		      required: true,
		      email: true
		    },
		    comentarios: {
		    	required: true
		    } 
		  },
		  messages: {
		    nombre: {
		    	required: "Debe ingresar su Nombre"
		    },
		    telefono: {
		    	required: "Debe ingresar su Teléfono"
		    },		    
		    email: {
		      required: "Debe ingresar su Email",
		      email: "Debe ingresar una dirección válida"
		    },	 
			comentarios: {
			  	required: "Debe ingresar su comentario."
			  }
			}
	});
	$('#contact button').click( function() {

		if ($("#contact").valid()) {

			var nombre = $("input#nombre").val();			
			var telefono = $("input#telefono").val();
			var email = $("input#email").val();
			var comentarios = $("textarea#comentarios").val();			
			$.ajax({
	            type: "POST",
	            url: 'bin/send_contact.php',
	            data:  {name_c: nombre, phone_c: telefono, email_c: email, comment_c: comentarios},
	            beforeSend: function () {
				//$('#homeform').hide();
				//$('.loading_form ').fadeIn();

				},
	            success: function(){
	            	$("input#nombre").val('');					
					$("input#telefono").val('');
					$("input#email").val('');
					$("textarea#comentarios").val('');
	            	$('.result-contact').html('Su mensaje ha sido envíado.').fadeIn();
	            },
	            error: function(){
	            	$('.result-contact').html('No se pudo enviar. Intente nuevamente.').fadeIn();
	            }
	        });
	        return false;
		}
	});


	//Validation Contact
	$("#solicitar").validate({
		  rules: {
		    solicitar_nombre:{
		    	required: true
		    },
			solicitar_email:{
				required: true,
		      	email: true		    	
		    },		    
		    solicitar_producto: {
		      required: true
		    },
		    solicitar_comentario: {
		    	required: true
		    } 
		  },
		  messages: {
		    solicitar_nombre: {
		    	required: "Debe ingresar su Nombre"
		    },
		    solicitar_email: {
		      required: "Debe ingresar su Email",
		      email: "Debe ingresar una dirección válida"
		    },		    
		    solicitar_producto: {
		    	required: "Debe ingresar un Producto"
		    },		    	 
			solicitar_comentario: {
			  	required: "Debe ingresar su comentario."
			  }
			}
	});
	$('#solicitar #send').click( function() {

		if ($("#solicitar").valid()) {

			var solicitar_nombre = $("input#solicitar_nombre").val();			
			var solicitar_email = $("input#solicitar_email").val();
			var solicitar_producto = $("input#solicitar_producto").val();
			var solicitar_comentarios = $("textarea#solicitar_comentario").val();			
			$.ajax({
	            type: "POST",
	            url: 'bin/send_contact_solicitar.php',
	            data:  {name_s: solicitar_nombre, email_s: solicitar_email, product_s: solicitar_producto, comment_s: solicitar_comentarios},
	            beforeSend: function () {
				//$('#homeform').hide();
				//$('.loading_form ').fadeIn();

				},
	            success: function(){
	            	$("input#solicitar_nombre").val('');					
					$("input#solicitar_email").val('');
					$("input#solicitar_producto").val('');
					$("textarea#solicitar_comentario").val('');
	            	$('.result-contact').html('Su mensaje ha sido envíado.').fadeIn();
	            },
	            error: function(){
	            	$('.result-contact').html('No se pudo enviar. Intente nuevamente.').fadeIn();
	            }
	        });
	        return false;
		}
	});


	/*
	$('.urnas .boton button').click( function() {		
		var nombre = $(this).attr('data-nombre');
		alert(nombre);
		var tipo = $(this).find('.urnas .block h4').text();	
		var llena_input = nombre+' - '+tipo;
		$('.modal #solicitar input#solicitar_producto').val(llena_input);	
	});
	*/

});
